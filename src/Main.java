public class Main {

    private static int[] firstArray;
    private static int[] temp;

    private static int count = 0;


    public static void main(String[] args) {
        firstArray = new int[]{2, -1, 4, 3, 5, 6};
        int[] secondArray = new int[]{1, 2, 6 , 5};

        //Случай, когда первый массив не отсортирован
        temp = new int[firstArray.length];
        mergeSort(0, firstArray.length);

        System.out.println(checkSameNumbers(firstArray, secondArray));
    }

    //Случай, когда первый массив отсортирован
    private static int checkSameNumbers(int[] firstArray, int[] secondArray) {
        for (int el : secondArray) {
            hasSameNumber(el, firstArray);
        }
        return count;
    }

    private static void hasSameNumber(int el, int[] a) {

        if (a[a.length / 2] == el) {
            count++;
            return;
        }

        if (a.length == 1){
            return;
        }

        int[] tmp;
        if (a[a.length / 2] < el) {
            tmp = new int[a.length - a.length / 2];
            System.arraycopy(a, a.length / 2, tmp, 0, a.length - a.length / 2);
            hasSameNumber(el, tmp);
        } else {
            tmp = new int[a.length / 2];
            System.arraycopy(a, 0, tmp, 0, a.length / 2);
            hasSameNumber(el, tmp);
        }
    }


    //Случай, когда первый массив не отсортирован
    private static void merge(int l, int m, int r) {
        int i = l;
        int j = m;
        for (int k = l; k < r; k++) {
            if (j == r || (i < m && firstArray[i] <= firstArray[j])) {
                temp[k] = firstArray[i];
                i++;
            } else {
                temp[k] = firstArray[j];
                j++;
            }
        }
        System.arraycopy(temp, l, firstArray, l, r - l);
    }

    private static void mergeSort(int l, int r) {
        if (r <= l + 1) return;
        int m = (l + r) >> 1;
        mergeSort(l, m);
        mergeSort(m, r);
        merge(l, m, r);
    }
}